import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SchoolGameApiRequestService } from 'api/requests-services/school-game-api-request.service';
import { IWordsDataset } from '../../models/words-dataset.model';

@Injectable({
  providedIn: 'root'
})
export class WordsDatasetService {

    private baseURL: string;

    constructor(private schoolgameAPIService: SchoolGameApiRequestService<any>) {
        this.baseURL = this.schoolgameAPIService.getApiURL();
    }

    public getWordsDatasetById(wordDatasetId: string): Observable<IWordsDataset> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + 'word-dataset/' + wordDatasetId,
                null
            );
    }

    public getWordsDatasetsByTeacherId(teacherId: string): Observable<IWordsDataset[]> {
        return this.schoolgameAPIService
            .request(
                'GET',
                this.baseURL + 'word-dataset?teacherId=' + teacherId,
                null
            );
    }

    public createWordsDataset(wordsDataset: IWordsDataset): Observable<IWordsDataset> {
        return this.schoolgameAPIService
            .request(
                'POST',
                this.baseURL + 'word-dataset',
                JSON.stringify(wordsDataset)
            );
    }

    public updateWordsDataset(wordsDataset: IWordsDataset): Observable<IWordsDataset> {
        return this.schoolgameAPIService
            .request(
                'PUT',
                this.baseURL + 'word-dataset/' + wordsDataset.id,
                JSON.stringify(wordsDataset)
            );
    }


    public deleteWordsDataset(wordsDatasetId: string): Observable<void> {
        return this.schoolgameAPIService
            .request(
                'DELETE',
                this.baseURL + 'word-dataset/' + wordsDatasetId,
                null
            );
    }
}
