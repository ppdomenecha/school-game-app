import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SchoolGameApiRequestService<T> {
    private baseURL: string;
    private headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': '*',
        'Allowed-Hosts': '*'
    };

    constructor(private http: HttpClient) {
        this.baseURL = environment.apiURL;
    }

    public getApiURL(): string {
        return this.baseURL;
    }

    public request(method: string, url: string, bodyObj: any, customHeaders?: any): Observable<T> {
        return this.http.request<any>(
            method,
            url,
            {
                body: bodyObj,
                headers: customHeaders ? customHeaders : this.headers
            });
    }

}
