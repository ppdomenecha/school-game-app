import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { LicensesComponent } from './public/licenses/licenses.component';
import { LoginComponent } from './public/login/login.component';
import { RegisterComponent } from './public/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptorService } from 'app/services/jwt-interceptor/jwt-interceptor.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { UnityComponent } from './unity/unity.component';

const routes: Routes = [
    {
        path: 'public/licenses',
        component: LicensesComponent,
        pathMatch: 'full'
    },
    {
        path: 'public/login',
        canActivate: [AuthGuardService],
        component: LoginComponent,
        pathMatch: 'full'
    },
    {
        path: 'public/register',
        canActivate: [AuthGuardService],
        component: RegisterComponent,
        pathMatch: 'full'
    },
    {
        path: 'teachers',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./pages/teachers-zone/teachers-zone.module').then(m => m.TeachersZoneModule)
    },
    {
        path: 'students',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./pages/students-zone/students-zone.module').then(m => m.StudentsZoneModule)
    },
    {
        path: '',
        pathMatch: 'full',
        // canActivate: [],
        redirectTo: 'public/login'
    }
];

@NgModule({
    declarations: [
        AppComponent,
        LicensesComponent,
        LoginComponent,
        RegisterComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes, {
            enableTracing: false,
            onSameUrlNavigation: 'reload'
        }),
        TranslateModule.forRoot(),
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatSlideToggleModule,
        FormsModule,
        MatTableModule,
        MatSortModule,
        NgbModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
    ],
    bootstrap: [AppComponent],
    exports: [
        RouterModule,
        MatSlideToggleModule,
        MatTableModule,
        MatSortModule,
        ReactiveFormsModule,
        FormsModule,
    ]
})
export class AppModule { }
