import { IWord } from './word.model';

export class IWordsDataset {
    id: string;
    teacherId: string;
    name: string;
    isPublic: boolean;
    words: IWord[];
    studentIds: string[];
}
