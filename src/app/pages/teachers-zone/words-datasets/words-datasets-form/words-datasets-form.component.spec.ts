import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordsDatasetsFormComponent } from './words-datasets-form.component';

describe('WordsDatasetsFormComponent', () => {
  let component: WordsDatasetsFormComponent;
  let fixture: ComponentFixture<WordsDatasetsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordsDatasetsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordsDatasetsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
