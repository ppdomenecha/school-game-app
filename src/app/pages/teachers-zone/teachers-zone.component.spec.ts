import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachersZoneComponent } from './teachers-zone.component';

describe('TeachersZoneComponent', () => {
  let component: TeachersZoneComponent;
  let fixture: ComponentFixture<TeachersZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachersZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachersZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
