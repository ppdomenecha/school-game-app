import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsZoneComponent } from './students-zone.component';

describe('StudentsZoneComponent', () => {
  let component: StudentsZoneComponent;
  let fixture: ComponentFixture<StudentsZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
