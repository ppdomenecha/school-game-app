import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'api/models/user.model';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
    }

    public ngOnInit(): void {
        this.loginForm = new FormGroup({
            email: new FormControl(''),
            password: new FormControl('')
        });
    }

    public onSubmit(): void{
        this.authenticationService
            .login(this.loginForm.value.email, this.loginForm.value.password)
            .toPromise()
            .then(
                (x: User) => {
                    x.role === 'TEACHER' ?
                        this.router.navigate(['/teachers']) :
                        this.router.navigate(['/students']);
                },
                (error: any) => {
                    console.dir(error);
                }
            );
    }

}
