import { IStudent } from './student.model';

export class IStudentJoinClassroomRequest {
    studentId: string;
    code: string;
    message: string;
}

export class IStudentClassroom {
    id: string;
    studentId: string;
    student: IStudent;
    classroomId: string;
    classroom: IStudent;
    studentClassroomStatusCode: number;
    message: string;
}
