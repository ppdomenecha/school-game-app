import { Component, OnInit, TemplateRef } from '@angular/core';
import { IStudentClassroom } from 'api/models/student-classroom';
import { Router } from '@angular/router';
import { StudentService } from 'api/requests-services/student/student.service';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { User } from 'api/models/user.model';
import { IStudent } from 'api/models/student.model';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-student-classroom-details',
    templateUrl: './student-classroom-details.component.html',
    styleUrls: ['./student-classroom-details.component.css']
})
export class StudentClassroomDetailsComponent implements OnInit {

    public titleBar = 'Detalles de la clase';
    public studentClassroom: IStudentClassroom;
    public student: IStudent;
    public isStudentClassroomLoaded = false;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private modalService: NgbModal,
        private studentService: StudentService
    ) {
    }

    public ngOnInit(): void {
        const currentUser: User = this.authenticationService.currentUserValue;
        this.studentService.getStudentByUserId(currentUser.id).toPromise()
            .then((student: IStudent) => {
                this.student = student;
                this.studentService.getStudentClasrromByStudentId(this.student.id).toPromise()
                    .then((studentClassroom: IStudentClassroom) => {
                        this.studentClassroom = studentClassroom;
                        this.isStudentClassroomLoaded = true;
                    }, (reason: any) => {
                        if (reason.status === 404) {
                            this.isStudentClassroomLoaded = true;
                        }
                    });
            });
    }

    public goToJoinClass(): void {
        this.router.navigate(['students/classroom/join']);
    }

    public openConfirmDeleteJoinClassroomRequest(name: TemplateRef<any>): void {
        this.modalService.open(name, { centered: true, scrollable: true });
    }

    public cancelDialog(dialog: NgbActiveModal): void {
        dialog.dismiss();
    }

    public async submitDeleteJoinClassroomRequestDialog(dialog: NgbActiveModal): Promise<void> {
        await this.studentService.deleteJoinClassroomRequest(this.studentClassroom.id).toPromise();
        dialog.close();
        location.reload();
    }
}
