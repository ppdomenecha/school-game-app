import { CommonComponentsModule } from 'app/components/common-components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassroomsComponent } from './classrooms.component';
import { ClassroomFormComponent } from './classroom-form/classroom-form.component';
import { ClassroomsListComponent } from './classrooms-list/classrooms-list.component';
import { RouterModule, Routes } from '@angular/router';
import { MatTableModule } from '@angular/material/table';

const routes: Routes = [
    {
        path: '',
        component: ClassroomsComponent,
        children: [
            {
                path: 'create',
                component: ClassroomFormComponent,
                pathMatch: 'full'
            },
            {
                path: ':classroomId/edit',
                component: ClassroomFormComponent,
                pathMatch: 'full'
            },
            {
                path: '',
                pathMatch: 'full',
                component: ClassroomsListComponent
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        MatTableModule,
        CommonComponentsModule,
    ],
    declarations: [
        ClassroomsComponent,
        ClassroomsListComponent,
        ClassroomFormComponent
    ]
})
export class ClassroomsModule { }
