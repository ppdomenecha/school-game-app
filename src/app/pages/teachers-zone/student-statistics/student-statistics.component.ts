import { Component, OnInit } from '@angular/core';
import { IStudentStatistics } from 'api/models/student-statistics.model';
import { StudentService } from 'api/requests-services/student/student.service';
import { StudentStatisticsService } from 'api/requests-services/student-statistics/student-statistics.service';
import { ActivatedRoute } from '@angular/router';
import { IStudent } from 'api/models/student.model';

@Component({
    selector: 'app-student-statistics',
    templateUrl: './student-statistics.component.html',
    styleUrls: ['./student-statistics.component.css']
})
export class StudentStatisticsComponent implements OnInit
{
    public student: IStudent;
    public studentStatistics: IStudentStatistics;

    private userId: string;

    constructor(
        private studentService: StudentService,
        private studentStatisticsService: StudentStatisticsService,
        activatedRoute: ActivatedRoute
    ) {
        this.userId = activatedRoute.snapshot.params.userId;
    }

    public ngOnInit(): void
    {
        this.studentService.getStudentByUserId(this.userId)
            .subscribe((student: IStudent) => this.student = student);
        this.studentStatisticsService.getStudentStatisticsByUserId(this.userId)
            .subscribe((studentStatistics: IStudentStatistics) => this.studentStatistics = studentStatistics);
    }
}
