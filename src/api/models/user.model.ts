export class User {
    id: string;
    password: string;
    role: string;
    email: string;
    token: string;
    name: string;
}
