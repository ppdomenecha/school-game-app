import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrazyWordsComponent } from './crazy-words.component';

describe('CrazyWordsComponent', () => {
  let component: CrazyWordsComponent;
  let fixture: ComponentFixture<CrazyWordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrazyWordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrazyWordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
