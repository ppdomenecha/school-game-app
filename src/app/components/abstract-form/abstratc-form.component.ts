import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

export abstract class AbstractFormComponent {

    public formGroup: FormGroup;
    public titleBar: string;
    public isAnEdit: boolean;

    constructor(
        activatedRoute: ActivatedRoute
    ) {
        this.isAnEdit = activatedRoute.snapshot.routeConfig.path !== 'create';
    }

    public abstract submitFormAction(): void;
}
