import { ClassroomService } from 'api/requests-services/classroom/classroom.service';
import { EMPTY_GUID } from 'app/utils/constants';
import { IClassroom } from 'api/models/classroom.model';
import { AbstractFormComponent } from 'app/components/abstract-form/abstratc-form.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Clipboard } from '@angular/cdk/clipboard';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { TeacherService } from 'api/requests-services/teacher/teacher.service';
import { User } from 'api/models/user.model';
import { ITeacher } from 'api/models/teacher.model';

@Component({
    selector: 'app-classroom-form',
    templateUrl: './classroom-form.component.html',
    styleUrls: ['./classroom-form.component.css']
})
export class ClassroomFormComponent extends AbstractFormComponent implements OnInit {

    public classroomId: string;
    public classroom: IClassroom;
    private teacherId: string;
    private teacher: ITeacher;

    constructor(
        private router: Router,
        private clipboard: Clipboard,
        private classroomService: ClassroomService,
        private authenticationService: AuthenticationService,
        private teacherService: TeacherService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
        this.titleBar = this.isAnEdit ? 'Editar clase' : 'Nueva clase';
        if (this.isAnEdit) {
            this.classroomId = activatedRoute.snapshot.params.classroomId;
        }
    }

    public ngOnInit(): void {
        this.getCurrentTeacher();
    }

    public generateCode(): void {
        // TODO: if isAnEdit -> Abrir modal de confirmación. OK --> pedir al API el codigo
        this.classroomService.generateClassroomCode().toPromise()
            .then((response: any) => this.formGroup.get('code').setValue(response.code));
    }

    public copyCodeToClipboard(): void {
        this.clipboard.copy(this.formGroup.get('code').value);
    }

    public cancelFormAction(): void {
        this.router.navigate(['teachers/classrooms']);
    }

    public submitFormAction(): void {
        if (this.formGroup.valid) {
            const controls = this.formGroup.controls;
            const classroom: IClassroom = {
                id: controls.id.value,
                name: controls.name.value,
                description: controls.description.value,
                schoolGrade: controls.schoolGrade.value,
                schoolName: controls.schoolName.value,
                code: controls.code.value,
                teachersIds: controls.teachersIds.value
            };
            if (this.isAnEdit){
                this.classroomService.editClassroom(classroom).toPromise().then(
                    () => this.router.navigate(['teachers/classrooms']),
                    (error: any) => console.dir(error)
                );
            }
            else {
                this.classroomService.createClassroom(classroom).toPromise().then(
                    () => this.router.navigate(['teachers/classrooms']),
                    (error: any) => console.dir(error)
                );
            }
        }
        else {
        }
    }

    private async initializeForm(): Promise<void> {
        if (this.isAnEdit) {
            await this.loadClassroom();
        }
        this.formGroup = new FormGroup({
            id: new FormControl(this.isAnEdit ? this.classroom.id : EMPTY_GUID),
            name: new FormControl(
                this.isAnEdit ? this.classroom.name : '',
                [Validators.required, Validators.minLength(5), Validators.maxLength(100)]
            ),
            schoolName: new FormControl(this.isAnEdit ? this.classroom.schoolName : '', Validators.maxLength(100)),
            schoolGrade: new FormControl(this.isAnEdit ? this.classroom.schoolGrade : '', Validators.maxLength(100)),
            description: new FormControl(this.isAnEdit ? this.classroom.description : '', Validators.maxLength(4000)),
            teachersIds: new FormArray(this.isAnEdit ? this.getTeachersIdsFormControl() : [new FormControl(this.teacherId)]),
            code: new FormControl(
                {
                    value: this.isAnEdit ? this.classroom.code : this.generateCode(),
                    disabled: true
                },
                [Validators.required, Validators.minLength(15), Validators.maxLength(15)]
            ),
        });
        return Promise.resolve();
    }

    private getCurrentTeacher(): void {
        const currentUser: User = this.authenticationService.currentUserValue;
        this.teacherService.getTeacherByUserId(currentUser.id).toPromise()
            .then(async (teacher: ITeacher) => {
                this.teacher = teacher;
                this.teacherId = this.teacher.id;
                await this.initializeForm();
            });
    }

    private async loadClassroom(): Promise<void> {
        this.classroom = await this.classroomService.getClassroom(this.classroomId).toPromise();
        return Promise.resolve();
    }

    private getTeachersIdsFormControl(): FormControl[] {
        const studentsFormControl: FormControl[] = [];
        if (this.classroom.teachersIds) {
            this.classroom.teachersIds.forEach((teacherId: string) => {
                studentsFormControl.push(new FormControl(teacherId));
            });
        }
        return studentsFormControl;
    }
}
