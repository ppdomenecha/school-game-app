import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeachersDashboardComponent } from 'app/pages/teachers-zone/teachers-dashboard/teachers-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { TeachersZoneComponent } from './teachers-zone.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonComponentsModule } from 'app/components/common-components.module';
import { StudentClassroomRequestComponent } from 'app/pages/teachers-zone/student-classroom-request/student-classroom-request.component';
import { StudentStatisticsComponent } from 'app/pages/teachers-zone/student-statistics/student-statistics.component';
import { StatisticsChartsComponent } from './student-statistics/parts/statistics-charts/statistics-charts.component';
import { StatisticsHeaderComponent } from './student-statistics/parts/statistics-header/statistics-header.component';

const routes: Routes = [
    {
        path: '',
        component: TeachersZoneComponent,
        children: [
            {
                path: 'dashboard',
                component: TeachersDashboardComponent,
                pathMatch: 'full'
            },
            {
                path: 'classrooms',
                loadChildren: () => import('./classrooms/classrooms.module').then(m => m.ClassroomsModule)
            },
            {
                path: 'words-datasets',
                loadChildren: () => import('./words-datasets/words-datasets.module').then(m => m.WordsDatasetsModule)
            },
            {
                path: 'student-classroom-requests',
                component: StudentClassroomRequestComponent,
                pathMatch: 'full'
            },
            {
                path: 'student-statistics/:userId',
                component: StudentStatisticsComponent,
                pathMatch: 'full'
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'dashboard'
            }
        ]
    },
];

@NgModule({
    declarations: [
        TeachersDashboardComponent,
        TeachersZoneComponent,
        StudentClassroomRequestComponent,
        StudentStatisticsComponent,
        StatisticsHeaderComponent,
        StatisticsChartsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        CommonComponentsModule,
    ]
})
export class TeachersZoneModule { }
