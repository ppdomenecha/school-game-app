
export class ITeacher {
    id: string;
    name: string;
    userId: string;
}
