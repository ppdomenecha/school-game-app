import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { ITeacher } from 'api/models/teacher.model';
import { IWordsDataset } from 'api/models/words-dataset.model';
import { AbstractFormComponent } from 'app/components/abstract-form/abstratc-form.component';
import { WordsDatasetService } from 'api/requests-services/words-dataset/words-dataset.service';
import { ArasaacService } from 'api/requests-services/arasaac-service/arasaac.service';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { TeacherService } from 'api/requests-services/teacher/teacher.service';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { EMPTY_GUID } from 'app/utils/constants';
import { IStudent } from 'api/models/student.model';
import { ElementRef } from '@angular/core';
import { IWord } from 'api/models/word.model';
import { User } from 'api/models/user.model';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-words-datasets-form',
    templateUrl: './words-datasets-form.component.html',
    styleUrls: ['./words-datasets-form.component.scss']
})
export class WordsDatasetsFormComponent extends AbstractFormComponent implements OnInit {

    public wordDatasetId: string;
    public wordDataset: IWordsDataset;
    private teacherId: string;
    private teacher: ITeacher;
    private openedModal: NgbModalRef;

    public arrayFormGroupPosition: number;
    public searchTerm: string;
    public wordImages: string[];
    public selectable = true;
    public removable = true;
    public separatorKeysCodes: number[] = [];
    public studentsControl = new FormControl();
    public filteredStudents: IStudent[];
    public selectedStudents: IStudent[] = [];
    public availableStudents: IStudent[] = [];

    public wordsFormArray: FormArray;

    @ViewChild('studentInput') studentInput: ElementRef<HTMLInputElement>;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private teacherService: TeacherService,
        private wordDatasetService: WordsDatasetService,
        private arasaacService: ArasaacService,
        private modalService: NgbModal,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
        this.titleBar = this.isAnEdit ? 'Editar conjunto de palabras' : 'Nuevo conjunto de palabras';
        if (this.isAnEdit) {
            this.wordDatasetId = activatedRoute.snapshot.params.wordDatasetId;
        }
    }

    public ngOnInit(): void {
        this.getCurrentTeacher();
    }

    public submitFormAction(): void {
        if (this.formGroup.valid) {
            const controls = this.formGroup.controls;
            const wordsDataset: IWordsDataset = {
                id: controls.id.value,
                isPublic: false,
                name: controls.name.value,
                teacherId: controls.teacherId.value,
                studentIds: this.selectedStudents.map(s => s.id),
                words: (controls.words as FormArray).controls.map((w: FormGroup) => {
                    const word: IWord = {
                        id: w.controls.id.value,
                        imageUrl: w.controls.imageUrl.value,
                        value: w.controls.value.value,
                        wordDatasetId: w.controls.wordDatasetId.value
                    };
                    return word;
                })
            };
            if (this.isAnEdit) {
                this.wordDatasetService.updateWordsDataset(wordsDataset).toPromise().then(
                    () => this.router.navigate(['teachers/words-datasets']),
                    (error: any) => console.dir(error)
                );
            }
            else {
                this.wordDatasetService.createWordsDataset(wordsDataset).toPromise().then(
                    () => this.router.navigate(['teachers/words-datasets']),
                    (error: any) => console.dir(error)
                );
            }
        }
        else {
        }
    }

    public cancelFormAction(): void {
        this.router.navigate(['teachers/words-datasets']);
    }

    public addAsignedStudent(event: MatChipInputEvent): void {
        const value = (event.value || '').trim();
        const input = event.input;

        // Add
        if (value) {
            const student: IStudent = this.availableStudents.find(s => s.name === value);
            let index: number = this.selectedStudents.indexOf(student);
            if (index < 0) {
                index = this.availableStudents.indexOf(student);
                this.selectedStudents.push(...this.availableStudents.splice(index, 1));
            }
        }

        // Clear the input value
        if (input) {
            input.value = '';
        }

        this.studentsControl.setValue(null);
    }

    public removeAsignedStudent(student: IStudent): void {
        const index = this.selectedStudents.indexOf(student);

        if (index >= 0) {
            this.availableStudents.push(...this.selectedStudents.splice(index, 1));
        }

        this.studentsControl.updateValueAndValidity();
    }

    public selectedAsignedStudent(event: MatAutocompleteSelectedEvent): void {
        const value: IStudent = event.option.value;
        if (value) {
            const student: IStudent = this.availableStudents.find(s => s.name === value.name);
            const index: number = this.availableStudents.indexOf(student);
            if (index >= 0) {
                this.selectedStudents.push(...this.availableStudents.splice(index, 1));
                this.studentInput.nativeElement.value = '';
                this.studentsControl.setValue(null);
            }
        }
        this.studentsControl.updateValueAndValidity();
    }

    public addWordFormControl(): void {
        if (!this.wordsFormArray.controls.some((wf: FormGroup) => !wf.controls.value.value && !wf.controls.imageUrl.value)) {
            this.wordsFormArray.controls.unshift(this.getEmptyWordFormControl());
        }
    }

    public async searchInArasaac(searchTerm: string, modal: TemplateRef<any>, i: number): Promise<void> {
        this.searchTerm = searchTerm;
        this.arrayFormGroupPosition = i;
        this.wordImages = (await this.arasaacService.searchPictos(searchTerm).toPromise()).map(x => x._id);
        this.openedModal = this.modalService.open(modal, { centered: true, scrollable: true });
    }

    public selectImage(url: string): void {
        (this.wordsFormArray.controls[this.arrayFormGroupPosition] as FormGroup).controls.imageUrl.setValue(url);
        this.openedModal.close();
        this.openedModal = undefined;
    }

    public pluralChanged(i: number, checked: boolean): void {
        const plural = '&plural=true';
        let url: string = (this.wordsFormArray.controls[i] as FormGroup).controls.imageUrl.value;
        url = checked ? url + plural : url.replace(plural, '');
        (this.wordsFormArray.controls[i] as FormGroup).controls.imageUrl.setValue(url);
    }

    public pluralChecked(url: string): boolean {
        const plural = '&plural=true';
        return url.includes(plural);
    }

    public actionChanged(i: number, action: string): void {
        const past = '&action=past';
        const future = '&action=future';

        let url: string = (this.wordsFormArray.controls[i] as FormGroup).controls.imageUrl.value;
        url = url.replace(past, '').replace(future, '');

        switch (action) {
            case 'past': url += past; break;
            case 'future': url += future; break;
        }

        (this.wordsFormArray.controls[i] as FormGroup).controls.imageUrl.setValue(url);
    }

    public pastChecked(url: string): boolean {
        const past = '&action=past';
        return url.includes(past);
    }

    public presentChecked(url: string): boolean {
        return !this.pastChecked(url) && ! this.futureChecked(url);
    }

    public futureChecked(url: string): boolean {
        const future = '&action=future';
        return url.includes(future);
    }

    private async initializeForm(): Promise<void> {
        if (this.isAnEdit) {
            await this.loadWordDataset();
        }
        this.wordsFormArray = new FormArray(this.isAnEdit ? this.getWordsFormControl() : [this.getEmptyWordFormControl()]);
        this.formGroup = new FormGroup({
            id: new FormControl(this.isAnEdit ? this.wordDataset.id : EMPTY_GUID),
            name: new FormControl(
                this.isAnEdit ? this.wordDataset.name : '',
                [Validators.required, Validators.minLength(5), Validators.maxLength(100)]
            ),
            isPublic: new FormControl(false),
            // description: new FormControl(this.isAnEdit ? this.wordDataset.description : '', Validators.maxLength(4000)),
            teacherId: new FormControl(this.teacherId),
            studentIds: new FormControl(this.isAnEdit ? this.wordDataset.studentIds : []),
            words: this.wordsFormArray
        });
    }

    private getWordsFormControl(): FormGroup[] {
        const wordsFormControl: FormGroup[] = [];
        if (this.wordDataset.words) {
            this.wordDataset.words.forEach((word: IWord) => {
                wordsFormControl.push(new FormGroup({
                    id: new FormControl(word.id),
                    value: new FormControl(word.value, [Validators.required]),
                    imageUrl: new FormControl(word.imageUrl, [Validators.required]),
                    wordDatasetId: new FormControl(word.wordDatasetId),
                    plural: new FormControl()
                }));
            });
        }
        return wordsFormControl;
    }

    private getEmptyWordFormControl(): FormGroup {
        return new FormGroup({
            id: new FormControl(EMPTY_GUID),
            value: new FormControl('', [Validators.required]),
            imageUrl: new FormControl('', [Validators.required]),
            wordDatasetId: new FormControl(this.isAnEdit ? this.wordDataset.id : EMPTY_GUID),
            plural: new FormControl()
        });
    }

    private getCurrentTeacher(): void {
        const currentUser: User = this.authenticationService.currentUserValue;
        this.teacherService.getTeacherByUserId(currentUser.id).toPromise()
            .then(async (teacher: ITeacher) => {
                this.teacher = teacher;
                this.teacherId = this.teacher.id;
                this.availableStudents = await this.teacherService.getStudentsByTeacherId(this.teacher.id).toPromise();
                await this.initializeForm();
            });
    }

    private async loadWordDataset(): Promise<void> {
        this.wordDataset = await this.wordDatasetService.getWordsDatasetById(this.wordDatasetId).toPromise();
        this.wordDataset.studentIds.forEach((studentId: string) => {
            const student: IStudent = this.availableStudents.find(s => s.id === studentId);
            let index: number = this.selectedStudents.indexOf(student);
            if (index < 0) {
                index = this.availableStudents.indexOf(student);
                this.selectedStudents.push(...this.availableStudents.splice(index, 1));
            }
        });

    }

    public filterAvailableStudent(): void {
        this.filteredStudents = this.availableStudents.filter(s => s.name.toLowerCase().includes(this.studentInput.nativeElement.value.toLowerCase().trim()));
    }
}
