import { TestBed } from '@angular/core/testing';

import { StudentStatisticsService } from './student-statistics.service';

describe('StudentStatisticsService', () => {
  let service: StudentStatisticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentStatisticsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
