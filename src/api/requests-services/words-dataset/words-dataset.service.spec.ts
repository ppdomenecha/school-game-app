import { TestBed } from '@angular/core/testing';

import { WordsDatasetService } from './words-dataset.service';

describe('WordsDatasetService', () => {
  let service: WordsDatasetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WordsDatasetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
