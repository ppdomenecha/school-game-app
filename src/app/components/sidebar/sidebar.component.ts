import { Component, Input, OnInit } from '@angular/core';

declare const $: any;
export const guidRegexString = '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}';
export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const TEACHER_ROUTES: RouteInfo[] = [
    { path: '/teachers/dashboard', title: 'Panel Principal', icon: 'dashboard', class: '' },

    { path: '/teachers/classrooms', title: 'Listado de Clases', icon: 'groups', class: '' },
    { path: '/teachers/classrooms/create', title: 'Alta de Clases', icon: 'groups', class: 'hidden' },
    { path: `/teachers/classrooms/${guidRegexString}/edit`, title: 'Edición de Clases', icon: 'groups', class: 'hidden' },

    { path: '/teachers/words-datasets', title: 'Conjuntos de palabras', icon: 'widgets', class: '' },
    { path: '/teachers/words-datasets/create', title: 'Alta Conjuntos de palabras', icon: 'widgets', class: 'hidden' },
    { path: `/teachers/words-datasets/${guidRegexString}/edit`, title: 'Edición Conjuntos de palabras', icon: 'widgets', class: 'hidden' },

    { path: '/teachers/student-classroom-requests', title: 'Solicitudes de ingreso', icon: 'person_add', class: '' },

    { path: `/teachers/student-statistics/${guidRegexString}`, title: 'Estadísticas Alumno', icon: 'groups', class: 'hidden' },
    // { path: '/teachers/user-profile', title: 'User Profile',  icon: 'person', class: '' },
    // { path: '/teachers/table-list', title: 'Table List',  icon: 'content_paste', class: '' },
    // { path: '/teachers/typography', title: 'Typography',  icon: 'library_books', class: '' },
    // { path: '/teachers/icons', title: 'Icons',  icon: 'bubble_chart', class: '' },
    // { path: '/teachers/maps', title: 'Maps',  icon: 'location_on', class: '' },
    // { path: '/teachers/notifications', title: 'Notifications',  icon: 'notifications', class: '' },
    // { path: '/teachers/upgrade', title: 'Upgrade to PRO',  icon: 'unarchive', class: 'active-pro' },
];

export const STUDENT_ROUTES: RouteInfo[] = [
    { path: '/students/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
    { path: '/students/classroom', title: 'Clase',  icon: 'groups', class: '' },
    { path: '/students/games/crazy-words', title: 'Juegos',  icon: 'games', class: '' },
    // { path: '/students/user-profile', title: 'User Profile',  icon: 'person', class: '' },
    // { path: '/students/table-list', title: 'Table List',  icon: 'content_paste', class: '' },
    // { path: '/students/typography', title: 'Typography',  icon: 'library_books', class: '' },
    // { path: '/students/icons', title: 'Icons',  icon: 'bubble_chart', class: '' },
    // { path: '/students/maps', title: 'Maps',  icon: 'location_on', class: '' },
    // { path: '/students/notifications', title: 'Notifications',  icon: 'notifications', class: '' },
    // { path: '/students/upgrade', title: 'Upgrade to PRO',  icon: 'unarchive', class: 'active-pro' },
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    public menuItems: any[];
    @Input() public zoneName: string;

    constructor() { }

    public ngOnInit(): void {
        switch (this.zoneName) {
            case 'TEACHER':
                this.menuItems = TEACHER_ROUTES.filter(menuItem => menuItem && menuItem.class !== 'hidden');
                break;
            case 'STUDENT':
                this.menuItems = STUDENT_ROUTES.filter(menuItem => menuItem);
                break;
        }
    }
    public isMobileMenu(): boolean {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }
}
