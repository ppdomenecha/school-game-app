import { Component, Input } from '@angular/core';
import { IStudentStatistics } from 'api/models/student-statistics.model';

@Component({
    selector: 'app-statistics-parts-header',
    templateUrl: './statistics-header.component.html',
    styleUrls: ['./statistics-header.component.css']
})
export class StatisticsHeaderComponent {

    @Input() public data: IStudentStatistics;
    constructor() { }

    public getStringTime(seconds: number): string {
        const days = Math.floor(seconds / 60 / 60 / 24);
        const hours = Math.floor((seconds - days * 24 * 60 * 60) / 60 / 60);
        const mins = Math.floor((seconds - days * 24 * 60 * 60 - hours * 60 * 60) / 60);
        const secs = Math.floor(seconds - days * 24 * 60 * 60 - hours * 60 * 60 - mins * 60);

        return (days > 0 ? days + 'd' : '') +
            (hours > 0 ? hours + 'h' : '') +
            (mins > 0 ? mins + 'min' : '') +
            secs + 's';
    }

}
