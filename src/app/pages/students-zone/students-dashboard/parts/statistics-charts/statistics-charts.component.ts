import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Bar, IBarChartOptions, IChartistBarChart, IChartistBase, IChartistData, IChartistLineChart, ILineChartOptions, Interpolation, Line, plugins, Svg } from 'chartist';
import { IStudentStatistics } from 'api/models/student-statistics.model';
import 'chartist-plugin-legend';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-student-statistics-parts-charts',
    templateUrl: './statistics-charts.component.html',
    styleUrls: ['./statistics-charts.component.scss']
})
export class StatisticsChartsComponent implements OnInit {

    @Input() public data: IStudentStatistics;

    public selectedChartModalTitle: string;

    private selectedChartDetail: IChartistBase<IBarChartOptions>;
    private currentEndDistance: number;
    private currentDetailsData: IChartistData;

    constructor(
        private modalService: NgbModal
    ) {

    }

    public ngOnInit(): void {
        this.initPreviewDurationchart();
        this.initPreviewWinLosechart();
    }

    public showDurationChartDetails(chartDetailModal: TemplateRef<any>): void {
        this.selectedChartModalTitle = 'Tiempo de juego';
        this.modalService.open(chartDetailModal, { centered: true, scrollable: true, size: 'lg' });

        this.currentEndDistance = 0;

        const { data, options } = this.getDurationChartSliced(7, this.currentEndDistance, '300px');

        this.currentDetailsData = data;

        this.selectedChartDetail = new Line('#selectedChartDetail', data, options);

        // this.startAnimationForLineChart(this.selectedChartDetail as IChartistLineChart);
    }

    public showWinLoseChartDetails(chartDetailModal: TemplateRef<any>): void {
        this.selectedChartModalTitle = 'Victorias / Derrotas';
        this.modalService.open(chartDetailModal, { centered: true, scrollable: true, size: 'lg' });

        this.currentEndDistance = 0;

        const { data, options } = this.getWinLoseChartSliced(7, this.currentEndDistance, '300px');

        this.currentDetailsData = data;

        this.selectedChartDetail = new Bar('#selectedChartDetail', data, options);

        // this.startAnimationForBarChart(this.selectedChartDetail as IChartistBarChart);
    }

    public moveLeftWinLoseChartDetails(): void {
        if (this.currentEndDistance < this.currentDetailsData.labels.length) {
            this.currentEndDistance += Math.min(7, this.currentDetailsData.labels.length - this.currentEndDistance);
            const { data, options } = this.getWinLoseChartSliced(7, this.currentEndDistance, '300px');
            this.selectedChartDetail.update(data, options, true);
        }
    }

    public moveRigthWinLoseChartDetails(): void {
        if (this.currentEndDistance > 0) {
            this.currentEndDistance -= Math.min(7, this.currentEndDistance);
            const { data, options } = this.getWinLoseChartSliced(7, this.currentEndDistance, '300px');
            this.selectedChartDetail.update(data, options, true);
        }
    }

    public moveLeftDurationChartDetails(): void {
        if (this.currentEndDistance < this.currentDetailsData.labels.length) {
            this.currentEndDistance += Math.min(7, this.currentDetailsData.labels.length - this.currentEndDistance);
            const { data, options } = this.getDurationChartSliced(7, this.currentEndDistance, '300px');
            this.selectedChartDetail.update(data, options, true);
        }
    }

    public moveRigthDurationChartDetails(): void {
        if (this.currentEndDistance > 0) {
            this.currentEndDistance -= Math.min(7, this.currentEndDistance);
            const { data, options } = this.getDurationChartSliced(7, this.currentEndDistance, '300px');
            this.selectedChartDetail.update(data, options, true);
        }
    }

    private initPreviewDurationchart(): void {
        const sliceNumber = 5;
        const { data, options } = this.getDurationChartSliced(sliceNumber, 0, '150px');

        const gameplayDurationChart = new Line('#gameplayDuration', data, options);

        this.startAnimationForLineChart(gameplayDurationChart);
    }

    private initPreviewWinLosechart(): void {
        const sliceNumber = 5;
        const { data, options } = this.getWinLoseChartSliced(sliceNumber, 0, '150px');

        const websiteViewsChart = new Bar('#winLoseChart', data, options);

        this.startAnimationForBarChart(websiteViewsChart);
    }

    private getDurationChartSliced(sliceNumber: number, endDistance: number, chartHeight: string): { data: IChartistData; options: IBarChartOptions; } {
        const timestamps = this.getSortedTimestampsFromValues(this.data.durationStatistics.values);

        const durationSeries = this.getDurationSeries(timestamps);

        const endIndex = Object.keys(durationSeries).length - endDistance;
        const startIndex = Math.max(endIndex - Math.min(sliceNumber, endIndex), 0);

        const durationValues = Object.values(durationSeries).slice(startIndex, endIndex);

        const dataChart: IChartistData = this.getDurationChartistData(durationSeries, startIndex, endIndex, durationValues);
        const optionsChart: ILineChartOptions = this.getLineChartOptions(durationValues, chartHeight);
        return { data: dataChart, options: optionsChart };
    }

    private getWinLoseChartSliced(sliceNumber: number, endDistance: number, chartHeight: string): { data: IChartistData; options: IBarChartOptions; } {
        const timestamps = this.getSortedTimestampsFromValues(this.data.winLosseStatistics.values);

        const winLoseSeries = this.getWinLoseSeries(timestamps);

        const endIndex = Object.keys(winLoseSeries).length - endDistance;
        const startIndex = Math.max(endIndex - Math.min(sliceNumber, endIndex), 0);

        const winsLossesValues = Object.values(winLoseSeries).slice(startIndex, endIndex);

        const datawebsiteViewsChart: IChartistData = this.getWinLoseChartistData(winLoseSeries, startIndex, endIndex, winsLossesValues);
        const optionswebsiteViewsChart: IBarChartOptions = this.getBarChartOptions(winsLossesValues, chartHeight);
        return { data: datawebsiteViewsChart, options: optionswebsiteViewsChart };
    }

    private getSortedTimestampsFromValues(values: { [key: string]: any; }): string[] {
        const timestamps = Object.keys(values);
        timestamps.sort();
        return timestamps;
    }

    private getDurationSeries(timestamps: string[]): number[] {
        const reducedKeys = timestamps.map(k => ({ reduced: k.slice(0, 10), original: k }));

        const durationSeries = reducedKeys.reduce((acc, curr) => {
            if (acc[curr.reduced]) {
                acc[curr.reduced] += this.data.durationStatistics.values[curr.original];
            }
            else {
                acc[curr.reduced] = this.data.durationStatistics.values[curr.original];
            }
            return acc;
        }, [] as number[]);

        return durationSeries;
    }

    private getWinLoseSeries(timestamps: string[]): { wins: number; losses: number; }[] {
        const reducedKeys = timestamps.map(k => ({ reduced: k.slice(0, 10), original: k }));

        const winLoseSeries = reducedKeys.reduce((acc, curr) => {
            if (acc[curr.reduced]) {
                acc[curr.reduced].wins += this.data.winLosseStatistics.values[curr.original] ? 1 : 0;
                acc[curr.reduced].losses += this.data.winLosseStatistics.values[curr.original] ? 0 : 1;
            }
            else {
                acc[curr.reduced] = {};
                acc[curr.reduced].wins = this.data.winLosseStatistics.values[curr.original] ? 1 : 0;
                acc[curr.reduced].losses = this.data.winLosseStatistics.values[curr.original] ? 0 : 1;
            }
            return acc;
        }, [] as { wins: number; losses: number; }[]);
        return winLoseSeries;
    }

    private getLineChartOptions(durationValues: number[], chartHeight: string): ILineChartOptions {
        return {
            lineSmooth: Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: Math.max(...durationValues),
            height: chartHeight,
            axisY: {
                onlyInteger: true
            },
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0 },
            plugins: [
                plugins.legend()
            ]
        };
    }

    private getBarChartOptions(winsLossesValues: { wins: number; losses: number; }[], chartHeight: string): IBarChartOptions {
        return {
            axisX: {
                showGrid: false,
            },
            low: 0,
            height: chartHeight,
            axisY: {
                onlyInteger: true
            },
            high: Math.max(...winsLossesValues.map(x => x.wins), ...winsLossesValues.map(x => x.losses)),
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0 },
            plugins: [
                plugins.legend()
            ]
        };
    }

    private getDurationChartistData(
        durationSeries: number[],
        startIndex: number,
        endIndex: number,
        durationValues: number[]
    ): IChartistData
    {
        return {
            labels: Object.keys(durationSeries).slice(startIndex, endIndex),
            series: [{ name: 'Tiempo de juego (s)', data: durationValues }]
        };
    }

    private getWinLoseChartistData(
        winLoseSeries: { wins: number; losses: number; }[],
        startIndex: number,
        endIndex: number,
        winsLossesValues: { wins: number; losses: number; }[]
    ): IChartistData
    {
        return {
            labels: Object.keys(winLoseSeries).slice(startIndex, endIndex),
            series: [
                { name: 'Victorias', data: winsLossesValues.map(x => x.wins) },
                { name: 'Derrotas', data: winsLossesValues.map(x => x.losses) }
            ]
        };
    }

    private startAnimationForLineChart(chart: IChartistLineChart): void {
        let seq: any;
        let delays: any;
        let durations: any;
        seq = 0;
        delays = 80;
        durations = 500;

        chart.on('draw', (data: any) => {
            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: Svg.Easing.easeOutQuint
                    }
                });
            } else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });

        seq = 0;
    }

    private startAnimationForBarChart(chart: IChartistBarChart): void {
        let seq = 0;
        const delays = 80;
        const durations = 500;

        chart.on('draw', (data: any) => {
            if (data.type === 'bar') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });
    }
}
