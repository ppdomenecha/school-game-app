import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordsDatasetsListComponent } from './words-datasets-list.component';

describe('WordsDatasetsListComponent', () => {
  let component: WordsDatasetsListComponent;
  let fixture: ComponentFixture<WordsDatasetsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordsDatasetsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordsDatasetsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
