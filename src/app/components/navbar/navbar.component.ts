import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { TEACHER_ROUTES, STUDENT_ROUTES, RouteInfo } from 'app/components/sidebar/sidebar.component';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    private listTitles: RouteInfo[];
    private location: Location;
    private mobileMenuVisible: any = 0;
    private toggleButton: any;
    private sidebarVisible: boolean;

    @Input() public zoneName: string;

    constructor(
        location: Location,
        private element: ElementRef,
        private router: Router,
        private autenticationService: AuthenticationService
    ) {
        this.location = location;
        this.sidebarVisible = false;
    }

    public ngOnInit(): void {
        switch (this.zoneName) {
            case 'TEACHER':
                this.listTitles = TEACHER_ROUTES.filter(listTitle => listTitle);
                break;
            case 'STUDENT':
                this.listTitles = STUDENT_ROUTES.filter(listTitle => listTitle);
                break;
        }

        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this.router.events.toPromise().then(_ => {
            this.sidebarClose();
            const $layer: any = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
                this.mobileMenuVisible = 0;
            }
        });
    }

    public sidebarOpen(): void {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(() => {
            toggleButton.classList.add('toggled');
        }, 500);

        body.classList.add('nav-open');

        this.sidebarVisible = true;
    }

    public sidebarClose(): void {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    }

    public sidebarToggle(): void {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        const $toggle = document.getElementsByClassName('navbar-toggler')[0];
        let $layer;

        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
        const body = document.getElementsByTagName('body')[0];

        if (this.mobileMenuVisible === 1) {
            // $('html').removeClass('nav-open');
            body.classList.remove('nav-open');
            if ($layer) {
                $layer.remove();
            }
            setTimeout(() => {
                $toggle.classList.remove('toggled');
            }, 400);

            this.mobileMenuVisible = 0;
        } else {
            setTimeout(() => {
                $toggle.classList.add('toggled');
            }, 430);

            $layer = document.createElement('div');
            $layer.setAttribute('class', 'close-layer');

            if (body.querySelectorAll('.main-panel')) {
                document.getElementsByClassName('main-panel')[0].appendChild($layer);
            } else if (body.classList.contains('off-canvas-sidebar')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }

            setTimeout(() => {
                $layer.classList.add('visible');
            }, 100);

            $layer.onclick = (() => { // asign a function
                body.classList.remove('nav-open');
                this.mobileMenuVisible = 0;
                $layer.classList.remove('visible');
                setTimeout(() => {
                    $layer.remove();
                    $toggle.classList.remove('toggled');
                }, 400);
            }).bind(this);

            body.classList.add('nav-open');
            this.mobileMenuVisible = 1;

        }
    }

    public getTitle(): any {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }

        for (const item of this.listTitles) {
            const regex = new RegExp(item.path + '$');
            if (regex.test(titlee)) {
                return item.title;
            }
        }
        return 'Dashboard';
    }

    public logoutUser(): void {
        this.autenticationService.logout();
        this.router.navigate(['public/login']);
    }
}
