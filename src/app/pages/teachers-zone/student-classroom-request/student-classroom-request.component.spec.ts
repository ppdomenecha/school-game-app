import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentClassroomRequestComponent } from './student-classroom-request.component';

describe('StudentClassroomRequestComponent', () => {
  let component: StudentClassroomRequestComponent;
  let fixture: ComponentFixture<StudentClassroomRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentClassroomRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentClassroomRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
