import { AuthenticationService } from 'api/requests-services/authentication/authentication.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { Router } from '@angular/router';
import { User } from 'api/models/user.model';
import { UserService } from 'api/requests-services/user/user.service';
import { EMPTY_GUID } from 'app/utils/constants';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    @ViewChild('isTeacherToggle', { static: false }) public isTeacherToggle: MatSlideToggle;

    public registerForm: FormGroup;

    constructor(
        private router: Router,
        private userService: UserService,
        private authenticationService: AuthenticationService,
    ) {
     }

    public ngOnInit(): void {
        this.registerForm = new FormGroup({
            email: new FormControl(''),
            password: new FormControl(''),
            confirmPassword: new FormControl(''),
            isTeacher: new FormControl(''),
            name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)])
        });
    }

    public onSubmit(): void {
        this.registerForm.updateValueAndValidity();
        console.log(this.registerForm.value);
        if (this.registerForm.valid) {
            const user = new User();
            user.id = EMPTY_GUID;
            user.password = this.registerForm.value.password;
            user.email = this.registerForm.value.email;
            user.role = this.registerForm.value.isTeacher ? 'TEACHER' : 'STUDENT';
            user.name = this.registerForm.value.name;
            this.userService
                .createUser(user)
                .toPromise()
                .then(() => {
                    this.authenticationService
                        .login(user.email, user.password)
                        .toPromise()
                        .then(x => x.role === 'TEACHER' ? this.router.navigate(['/teachers']) : this.router.navigate(['/students']));
                });
        }
        else {
            alert('Formulario inválido');
        }
    }
}
