import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordsDatasetsStudentAssignmentComponent } from './words-datasets-student-assignment.component';

describe('WordsDatasetsStudentAssignmentComponent', () => {
  let component: WordsDatasetsStudentAssignmentComponent;
  let fixture: ComponentFixture<WordsDatasetsStudentAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordsDatasetsStudentAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordsDatasetsStudentAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
